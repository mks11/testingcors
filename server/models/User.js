import mongoose from "mongoose";
const Schema = mongoose.Schema;

const userSchema = new Schema({
  firstName: String,
  lastName: String,
  username: {
    type: String,
    unique: true
  },
  password: {
    type: String
  }
})


const User = mongoose.model('User', userSchema);


export default User;