import express from "express";
import mongoose from "mongoose";
import User from "./models/User";
import bodyParser from "body-parser";
import cors from "cors";
import passport from "passport";
import bcrypt from "bcrypt";

const saltRounds = 10;

const LocalStrategy = require("passport-local").Strategy;

const  corsOptions = {
  origin: 'http://localhost:3000',
}

passport.use(
  new LocalStrategy(
    (username, password, done) => {
      console.log('LocalStrategy')
      User.findOne({ username: username }, (err, user) => {
        if (err) {
          return done(err);
        }

        if (!user) {
          return done(null, false, { message: "incorrect username" });
        }

        bcrypt.compare(password, user.password, (err, res) => {
          if (res === false){
            return done(null, false, { message: "incorrect password"})
          } else {
            return done(null, user);
          }
        })
      });
    }
  )
);

const app = express();


app.use(cors());
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use(require('express-session')({ secret: 'keyboard cat', resave: false, saveUninitialized: false }));
app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser((user, done)=>{
  console.log('serializing user')
  done(null, user.id)
})

passport.deserializeUser((id, done)=>{
  console.log('deserializing user')
  User.findById(id, (err, user)=>{
    done(err, user)
  })
})

mongoose.connect("mongodb://localhost/pass", {
  useMongoClient: true
});



app.get("/", (req, res) => {
  res.send("hello, world");
});


app.get("/hello", cors(corsOptions), (req, res)=>{
  console.log('hello')
  if(req.user){
    res.send('Authorized')
  } else {
    res.send('UnAuthorized')
  }
})


app.post('/login', 
  passport.authenticate('local', { failureRedirect: '/login' }),
  function(req, res) {
    res.redirect('/');
  });

app.get("/users", (req, res) => {
  User.find({}, (err, users) => {
    res.send(users);
  });
});

app.get("/user/:username", (req, res) => {
  User.findOne({ username: req.params.username }, (err, user) => {
    res.send(user);
  });
});

app.post("/user", (req, res) => {
  const firstName = req.body.firstName || "";
  const lastName = req.body.lastName || "";
  const username = req.body.username || "";
  const password = req.body.password || "";

  if (
    password.length < 8 ||
    username.length < 4 ||
    firstName.length < 1 ||
    lastName.length < 1
  ) {
    res.sendStatus(400);
  } else {
    bcrypt.hash(password, saltRounds, (err, hashed)=>{
      User.create(
      {
        username: username,
        password: hashed,
        firstName: firstName,
        lastName: lastName
      },
      (err, user) => {
        if (err) {
          res.sendStatus(500);
        } else {
          res.sendStatus(200);
        }
      }
    );
    })
  }
});

const PORT = 4000;
app.listen(PORT, () => {
  console.log("port is connect on ", PORT);
});
