import React, { Component } from 'react';


export default class Signup extends Component {
  
  constructor(props){
    super(props)
    this.handleOnSubmit = this.handleOnSubmit.bind(this)
  }

  handleOnSubmit(event){
    event.preventDefault()
    console.log('form submit clicked!', this.firstName.value, this.lastName.value)

    axios.post(SIGNUP_URL,{
      firstName: this.firstName.value,
      lastName: this.lastName.value,
      username: this.username.value,
      password: this.password.value
    }).then((res)=>{
      console.log("res", res)
    }).catch((err)=>{
      console.log("err", err)
    })
  }

  render(){
    return(
        <Form onSubmit={this.handleOnSubmit}>
          <Title> Sign Up </Title>
          <Label> Name </Label>
          <FieldSet>
            <FirstName type="text" placeholder="First Name" innerRef={(fn)=>this.firstName=fn}/>
            <LastName type="text" placeholder="Last Name" innerRef={(ln) => this.lastName=ln}/>
          </FieldSet>
          <FieldSet>
            <UserName type="text" placeholder="username" innerRef={(un)=>this.username=un}/>
            <Password type="password" placeholder="password" innerRef={(pwd)=>this.password=pwd}/>
          </FieldSet>
          <Submit type="submit"/>
        </Form>
      )
  }
}