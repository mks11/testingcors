import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import styled from "styled-components";
import axios from "axios";
import { LOGIN_URL, SIGNUP_URL, BASE_URL } from "./constants"
import $ from "jquery";



const Container = styled.div`
  width: 80%;
  margin-left: auto;
  margin-right: auto;
`

const Form = styled.form`
  border: 1px solid #eee;
  font-size: 1em;
  flex: 1;
  padding: 1em;
  margin-top: 20px;
`;

const Label = styled.label`
  font-size: 1.2em;
`

const Message = styled.div`
  padding: 0.5em;
  color: ${props => props.good?'green':'red'}
`

const FirstName = styled.input`
  padding: 0.5em;
  color: orange; 
  border: 1px solid gray;
  border-radius: 3px;
  margin: 0.5em;
  background-color: honeydew;
  font-size: 1em;
`;

const LastName = styled.input`
  padding: 0.5em;
  color: orange;
  border: 1px solid gray;
  margin: 0.5em;
  border-radius: 3px;
  font-size: 1em;
`;

const UserName = styled.input`
  padding: 0.5em;
  color: red;
  border: 1px solid ${props => props.badlogin?'red':'gray'};
  margin: 0.5em;
  border-radius: 3px;
    font-size: 1em;
`;

const Password = styled.input`
  padding: 0.5em;
  color: red;
  border: 1px solid ${props => props.badlogin?'red':'gray'};
  margin: 0.5em;
  border-radius: 3px;
  font-size: 1em;
`;

const FieldSet = styled.fieldset`
  padding: 0.5em;
  border: none;
`

const Submit = styled.input`
  font-size: 1.5em;
  border: 1px solid black;
  padding: 0.2em 0.8em;
  background-color: white;
  border-radius: 3px;
  color: black;
`

const Title = styled.h1`
  // font-size: 2em;
`

class Login extends Component {
  constructor(props){
    super(props)
    this.handleOnSubmit = this.handleOnSubmit.bind(this)

    this.state={
      badlogin: false
    }
  }

  handleOnSubmit(event){
    event.preventDefault()
    this.setState({
      badlogin: false
    })
    axios.post(LOGIN_URL, {
      username: this.username.value,
      password: this.password.value
    }).then((res)=>{
      console.log("res", res)
    }).catch((err)=>{
      console.log("err", err.response.status)
      const status = err.response && err.response.status
      if(err.response){
        if(status >= 400 && status < 500){
          this.setState({badlogin: true})
        }
      }
    })
  }

  render(){
    return(
        <Form onSubmit={this.handleOnSubmit}>
          <Title> Login </Title>
          <FieldSet>
            <UserName badlogin={this.state.badlogin} type="text" placeholder="username" innerRef={(un) => this.username = un}/>
            <Password badlogin={this.state.badlogin} type="password" placeholder="password" innerRef={(pwd) => this.password = pwd}/>
            <Message good={false}> { this.state.badlogin ? 'Incorrect Login or Password' : ''} </Message>
          </FieldSet>
          <Submit type="submit"/>
        </Form>
    )
  }
}

class Signup extends Component {
  
  constructor(props){
    super(props)
    this.handleOnSubmit = this.handleOnSubmit.bind(this)
  }

  handleOnSubmit(event){
    event.preventDefault()
    console.log('form submit clicked!', this.firstName.value, this.lastName.value)

    axios.post(SIGNUP_URL,{
      firstName: this.firstName.value,
      lastName: this.lastName.value,
      username: this.username.value,
      password: this.password.value
    }).then((res)=>{
      console.log("res", res)
    }).catch((err)=>{
      console.log("err", err)
    })
  }

  render(){
    return(
        <Form onSubmit={this.handleOnSubmit}>
          <Title> Sign Up </Title>
          <Label> Name </Label>
          <FieldSet>
            <FirstName type="text" placeholder="First Name" innerRef={(fn)=>this.firstName=fn}/>
            <LastName type="text" placeholder="Last Name" innerRef={(ln) => this.lastName=ln}/>
          </FieldSet>
          <FieldSet>
            <UserName type="text" placeholder="username" innerRef={(un)=>this.username=un}/>
            <Password type="password" placeholder="password" innerRef={(pwd)=>this.password=pwd}/>
          </FieldSet>
          <Submit type="submit"/>
        </Form>
      )
  }
}

const Button = styled.button`
  padding: 0.6em;
  color: black;
  background-color: white;
  font-size: 1em;
` 

class TestArea extends Component {
  
  constructor(props){
    super(props)
    this.state = {
      showMessage: 'old'
    }
    this.makeRequest = this.makeRequest.bind(this)
  }

  makeRequest(event){
    event.preventDefault();
    this.setState({
      showMessage: 'button clicked!'
    })

    // $.get(BASE_URL + '/hello',{
    //   credentials : "include"
    // })
    // .then((res) => {
    //   console.log(res)
    //   this.setState({
    //     showMessage: res.data
    //   })
    // })
    // .catch((err) => {
    //   console.log(err)
    // })
  
    $.ajax({
      url: BASE_URL + '/hello',
      xhrFields: {
        withCredentials: true
      }
    })
    .done(()=>{
      console.log('success')
    })
    .fail(()=>{
      console.log('fail')
    })
  }

  render(){
    return(
      <div> 
        <Message> {this.state.showMessage} </Message>
        <Button onClick={this.makeRequest}> Make Request </Button>
      </div>
      )
  }
}

class App extends Component {
  render() {
    return (
      <Container>
      <Title> Logged In As </Title>
        <Signup/>
        <Login/>
        <TestArea/>
      </Container>
    );
  }
}

export default App;
