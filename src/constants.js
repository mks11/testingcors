const BASE_URL = 'http://localhost:4000'
const LOGIN_URL = BASE_URL + '/login'
const SIGNUP_URL = BASE_URL + '/user'

export { BASE_URL, LOGIN_URL, SIGNUP_URL }